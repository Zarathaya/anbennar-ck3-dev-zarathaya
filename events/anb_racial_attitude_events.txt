﻿#Racial Preference Events
namespace = anb_racial_attitude

anb_racial_attitude.1 = {
	type = character_event
	hidden = no

	immediate = {

		#Make intolerant characters dislike this character if not same race
		every_in_list = {
			list = list_of_intolerant_characters

			if = {
				limit = {
					are_same_race = {
						CHARACTER = prev
						OTHER = this
					}
				}
				this = {
					reverse_add_opinion = {
						target = prev
						modifier = racial_preference_intolerant_opinion
					}
				}
			}
		}
	}
}