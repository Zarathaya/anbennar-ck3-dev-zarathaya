#County of Vivinmar
40 = { #Vivinmar

    # Misc
    culture = roilsardi
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History

}

#County of Loopuis
34 = { #Loopuis

    # Misc
    culture = roilsardi
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Saloren
32 = { #Saloren

    # Misc
    culture = roilsardi
    religion = house_of_minara
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

33 = { #Toarnaire

    # Misc
    culture = roilsardi
    religion = house_of_minara
    holding = city_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Roilsard
59 = { #Roilsard

    # Misc
    culture = roilsardi
    religion = court_of_adean
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Lanpool
47 = { #Lanpool

    # Misc
    culture = tretunic
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}

#County of Tretun
48 = { #Tretun

    # Misc
    culture = tretunic 
    religion = cult_of_the_dame
    holding = castle_holding
    #terrain = (doesn't seem to work at all)

    # History
}
