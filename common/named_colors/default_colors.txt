colors = {
	# used by coat of arms
	red 		= hsv {		0.02 	0.8 	0.45 	}
	blue 		= hsv {		0.58 	0.8 	0.4 	}
	yellow		= hsv {		0.1 	0.75 	0.75 	}
	green		= hsv {		0.35 	0.6 	0.30 	}
	black		= hsv {		0.1 	0.25 	0.12 	}
	white		= hsv {		0.08 	0.1 	0.75 	}
	purple 		= hsv {		0.9 	0.7 	0.35 	}
	orange 		= hsv {		0.064 	1 	0.6 	}
	
	grey 		= hsv {		0.0 	0.0 	0.50 	}
	brown 		= hsv360 {	021 	074 	045		}
	
	blue_light	= hsv {		0.58 	0.70 	0.55 	}
	green_light	= hsv {		0.35 	0.50 	0.40 	}
	yellow_light= hsv {		0.1 	0.80 	1.0 	}


	#Anbennar Colours
	# rgb {	28 124 140 }
	damerian_blue	= rgb {	28 124 140 }
	damerian_blue_light	= rgb {	39 160 163 }
	damerian_black	= rgb {	33 33 33 }
	damerian_white	= rgb {	209 211 212  }
	# damerian_white
	pearlsedge_blue	= rgb { 46 49 146 }
	pearlsedge_pearl = rgb { 254 248 223 }

	# moon_blue		= hsv360 {	021 	074 	045		}
	# silver			= hsv360 {	021 	074 	045		}

	# gawedi_blue		= hsv360 {	021 	074 	045		}
	gawedi_blue	=	rgb { 48  51  108 }

	busilari_orange = rgb { 211  85  4 } #Busilar's color in EU4
	eborthili_gold = rgb { 207  182  100 } #Eborthil's color in EU4
	verne_red = rgb { 150 0 0 } #Verne's color in EU4

	lorentish_red	= rgb {	237 28 36 }
	lorentish_red_dark	= rgb {	164 29 33 }
	lorentish_green	= rgb {	75 131 61 }
	lorentish_lilac	= rgb {	168 128 186 }
	lorentish_gold	= rgb { 255 239 159 }
	derannic_purple	= rgb { 102 45 145 }
	derannic_pink	= rgb { 218 28 92 }
	roilsardi_red	= rgb { 185 30 68 }
	roilsardi_green	= rgb { 176 229 104 }
	corvurian_red_dark	= rgb { 118 17 19 }
	corvurian_red_light	= rgb { 161 28 47 }
	iochand_yellow	= rgb { 255 203 32 }
	iochand_blue	= rgb { 58 50 153 }
	iochand_green	= rgb { 176 255 153 }
	wexonard_purple = rgb { 97  0  137 }
}

	