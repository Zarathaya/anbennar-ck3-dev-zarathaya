﻿#Effect for setting racial preference
set_racial_attitude_intolerant_effect = {
    set_variable = {
		name = racial_preference
		value = flag:intolerant
	}
}

set_racial_attitude_tolerant_effect = {
    set_variable = {
		name = racial_preference
		value = flag:tolerant
	}
}

set_racial_attitude_neutral_effect = {
	set_variable = {
		name = racial_preference
		value = flag:neutral
	}
}

#Master list of conditions for setting racial preference
set_racial_preference_general_effect = {
	#Sun elves are intolerant?
	if = {
		limit = { culture = culture:sun_elvish }
		random_list = {
			90 = {
				set_racial_attitude_intolerant_effect = yes
			}
			9 = {
				set_racial_attitude_neutral_effect = yes
			}
			1 = {
				set_racial_attitude_tolerant_effect = yes
			}
		}
	}

	#Half-elves are more tolerant?
	else_if = {
		limit = {
			has_trait = half_elf
		}
		random_list = {
			10 = {
				set_racial_attitude_intolerant_effect = yes
			}

			80 = {
				set_racial_attitude_tolerant_effect = yes
			}
			
			10 = {
				set_racial_attitude_neutral_effect = yes
			}
		}
	}

	#Equal split for rest of characters (change ratio?)
	else = {
		random_list = {
			33 = {
				set_racial_attitude_intolerant_effect = yes
			}
			33 = {
				set_racial_attitude_tolerant_effect = yes
			}
			33 = {
				set_racial_attitude_neutral_effect = yes
			}
		}
	}
}