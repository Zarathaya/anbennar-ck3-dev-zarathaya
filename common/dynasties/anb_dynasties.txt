﻿dynasty_silmuna = {
	name = "dynn_Silmuna"
	culture = "damerian"
}

dynasty_siloriel = {
	name = "dynn_Siloriel"
	culture = "lorentish"
}

dynasty_silgarion = {
	name = "dynn_Silgarion"
	culture = "damerian"
}

dynasty_silistra = {
	name = "dynn_Silistra"
	culture = "damerian"
}

dynasty_silurion = {
	name = "dynn_Silurion"
	culture = "damerian"
}

dynasty_silcalas = {
	name = "dynn_Silcalas"
	culture = "damerian"
}

dynasty_silebor = {
	name = "dynn_Silebor"
	culture = "damerian"
}

dynasty_silnara = {
	name = "dynn_Silnara"
	culture = "damerian"
}

dynasty_tretunis = {
	name = "dynn_Tretunis"
	culture = "tretunic"
}


9 = {
	name = "dynn_Pearlman"
	culture = "pearlsedger"
}

dynasty_roilsardis = {
	prefix = "dynnp_sil"
	name = "dynn_Roilsardis"
	culture = "roilsardi"
}

11 = {
	prefix = "dynnp_sil"
	name = "dynn_Vis"
	culture = "halfling"
}

12 = {
	prefix = "dynnp_sil"
	name = "dynn_Deranne"
	culture = "derannic"
}

dynasty_sorncost = {
	prefix = "dynnp_sil"
	name = "dynn_Sorncost"
	culture = "sorncosti"
}

14 = {
	name = "dynn_Lorentis"
	culture = "lorenti"
}

15 = {
	name = "dynn_jaherzuir"
	culture = "sun_elvish"
}

16 = {
	name = "dynn_Redstone"
	culture = "ruby_dwarvish"
}

18 = {
	prefix = "dynnp_sil"
	name = "dynn_Uelaire"
	culture = "damerian"
}

19 = {
	name = "dynn_Roysfort"
	culture = "halfling"
}

20 = {
	name = "dynn_Iochand"
	culture = "creek_gnomish"
}

dynasty_vernid = {
	name = "dynn_Vernid"
	culture = "vernman"
}

dynasty_iacoban = {
	name = "dynn_Iacobing"
	culture = "corvurian"
}

22 = {
	name = "dynn_Coddorran"
	culture = "cliff_gnomish"
}

23 = {
	name = "dynn_Askeling"
	culture = "reverian"
}

dynasty_gawe = {
	name = "dynn_Gawe"
	culture = "gawedi"
}

25 = {
	name = "dynn_Eaglecrest"
	culture = "gawedi"
}

26 = {
	name = "dynn_Mooring"
	culture = "moorman"
}

27 = {
	name = "dynn_taarthil"
	culture = "moon_elvish"
}

28 = {
	name = "dynn_tagalsheah"
	culture = "moon_elvish"
}

29 = {
	name = "dynn_Adshaw"
	culture = "old_alenic"
}

30 = {
	name = "dynn_Serpentsgard"
	culture = "blue_reachmen"
}

31 = {
	name = "dynn_Cobbler"
	culture = "blue_reachmen"
}

32 = {
	name = "dynn_Wex"
	culture = "wexonard"
}

33 = {
	name = "dynn_Sugambic"
	culture = "wexonard"
}

34 = {
	name = "dynn_Bisan"
	culture = "wexonard"
}

35 = {
	name = "dynn_Silverhammer"
	culture = "silver_dwarvish"
}

36 = {
	name = "dynn_Esmar"
	culture = "esmari"
}

37 = {
	name = "dynn_Bennon"
	culture = "esmari"
}

38 = {
	prefix = "dynnp_sil"
	name = "dynn_Estallen"
	culture = "ryalani"
}

39 = {
	name = "dynn_Ryalan"
	culture = "ryalani"
}

40 = {
	prefix = "dynnp_sil"
	name = "dynn_Leslinpar"
	culture = "esmari"
}

41 = {
	name = "dynn_Havoran"
	culture = "esmari"
}

42 = {
	name = "dynn_talunetein"
	culture = "moon_elvish"
}

43 = {
	name = "dynn_Farran"
	culture = "esmari"
}

44 = {
	name = "dynn_Eldman"
	culture = "crownsman"
}

45 = {
	name = "dynn_Vrorensson"
	culture = "white_reachmen"
}

46 = {
	name = "dynn_Ebonfrost"
	culture = "black_castanorian"
}

47 = {
	name = "dynn_Aldwoud"
	culture = "white_reachmen"
}

48 = {
	name = "dynn_Tederfremh"
	culture = "moon_elvish"
}

49 = {
	prefix = "dynnp_sil"
	name = "dynn_Cast"
	culture = "castanorian"
}

50 = {
	prefix = "dynnp_sil"
	name = "dynn_Anor"
	culture = "castanorian"
}

51 = {
	name = "dynn_Marr"
	culture = "marrodic"
}

52 = {
	name = "dynn_Balmire"
	culture = "old_alenic"
}

#100 - 200 : Jay
100 = {
	prefix = "dynnp_of"
	name = "dynn_Oldhaven"
	culture = "marcher"
}

dynasty_aubergentis = {
	name = "dynn_Aubergentis"
	culture = "lorenti"
}

dynasty_sidericis = {
	name = "dynn_Sidericis"
	culture = "damerian"
}

dynasty_acromis = {
	name = "dynn_Acromis"
	culture = "old_damerian"
}